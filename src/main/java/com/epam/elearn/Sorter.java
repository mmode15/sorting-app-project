package com.epam.elearn;

import java.util.Arrays;

/**
 * The {@code Sorter} class provides a small Java application that takes up to ten command-line arguments
 * as integer values, sorts them in the ascending order, and then prints them into standard output.
 *
 * <p><b>Note:</b> This class is designed to be used through the command line with 10 integer arguments.
 * If the number of arguments is not 10 or if any argument is not a valid integer, an error message
 * will be displayed, and the program will terminate.</p>
 *
 * <p><b>Usage:</b> java Sorter arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10</p>
 *
 * <p><b>Error Messages:</b>
 * <ul>
 *     <li>{@code Error: args length must be 10} - If the number of arguments is not 10.</li>
 *     <li>{@code Error: each arg must be a number} - If any argument is not a valid integer.</li>
 * </ul></p>
 *
 * <p><b>Sorting Algorithm:</b> The program uses the {@link Arrays#sort(int[])} method to sort the
 * provided integers in ascending order.</p>
 *
 * <p><b>Example:</b> If the program is executed with arguments {@code 5 3 7 1 9 2 4 8 6 10},
 * the output will be {@code Sorted numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}.</p>
 *
 * @author Maia Modebadze
 * @version 1.0
 */
public class Sorter {
    /**
     * The main method that is executed when the program is run.
     * it takes up to ten command-line arguments as integer values, sorts them in the ascending order,
     * and then prints them into standard output.
     *
     * @param args The command-line arguments containing 10 integers to be sorted.
     */
    public static void main(String[] args) {
        if (args.length != 10) {
            System.out.println("Error: args length must be 10");
            return;
        }
        int[] numbers = new int[args.length];

        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            System.out.println("Error: each arg must be a number");
            return;
        }
        Arrays.sort(numbers);
        System.out.println("Sorted numbers: " + Arrays.toString(numbers));
    }
}