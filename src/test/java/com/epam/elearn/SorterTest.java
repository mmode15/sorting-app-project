package com.epam.elearn;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class SorterTest extends TestCase {
    private String[] inputArgs;
    private String expectedOutput;

    @After
    public void clear(){
        System.setOut(System.out);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1", "5", "3", "7", "9", "2", "4", "8", "6", "10"}, "Sorted numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"},
                {new String[]{"5", "3", "7", "1", "9", "10", "123", "1231", "44", "3e"}, "Error: each arg must be a number"},
                {new String[]{}, "Error: args length must be 10"},
                {new String[]{"4"}, "Error: args length must be 10"},
                {new String[]{"as", "213"}, "Error: args length must be 10"},
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9"}, "Error: args length must be 10"},
        });
    }

    public SorterTest(String[] inputArgs, String expectedOutput) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
    }

    @Test
    public void testMain() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Sorter.main(inputArgs);

        assertEquals(expectedOutput, outContent.toString().trim());
    }
}